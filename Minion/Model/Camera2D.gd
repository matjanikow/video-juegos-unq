extends Camera2D

var player
var score
func _ready():
	score = 0
	player = get_node("../Player")
	self.set_position(Vector2(player.position.x, player.position.y))
	
func _process(delta):
	if(self.position.y > player.position.y):
		self.set_position(Vector2(self.position.x, player.position.y))
		score += 1