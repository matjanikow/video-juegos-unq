extends KinematicBody2D

const GRAVITY = 400
const WALK_SPEED = 350
var camera
var velocity = Vector2()
var popup
func _ready():
	camera = get_parent().get_node("Camera2D")
	popup = get_parent().get_node("CanvasLayer/Popup")
	pass
func _physics_process(delta):
    velocity.y += delta * GRAVITY
	
    if Input.is_action_pressed("ui_left"):
        velocity.x = -WALK_SPEED
    elif Input.is_action_pressed("ui_right"):
        velocity.x =  WALK_SPEED
    else:
        velocity.x = 0

    # We don't need to multiply velocity by delta because "move_and_slide" already takes delta time into account.

    # The second parameter of "move_and_slide" is the normal pointing up.
    # In the case of a 2D platformer, in Godot, upward is negative y, which translates to -1 as a normal.
    move_and_slide(velocity, Vector2(0, -1))
    var collision = move_and_collide(velocity * delta)
    if collision:
		#-250
	    velocity.y = -400
#    print (position.x)
	
func exit_screen():
	if position.y > camera.position.y + get_viewport().size.y/2:
		print("game over")
		popup.popup()
	if self.position.x > camera.position.x :
		self.position.x -= get_viewport().size.x + 50
	else :
		self.position.x += get_viewport().size.x + 100
		print(position.x)
	pass 